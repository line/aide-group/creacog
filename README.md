# creacog

This project aims to model the learner's cognition and behavior during the CreaCube task.

We are in a very preliminary phase, and are building fondational tools, as shared here:

Chloé Mercier, Frédéric Alexandre, and Thierry Viéville. *Reinforcement symbolic learning*. In ICANN’2021. Bratislava, Slovakia, September 2021. https://hal.inria.fr/hal-03327706 ([video short presentation](https://files.inria.fr:8443/mecsci/aide/icann'2021.mp4))

Chloé Mercier, Hugo Chateau-Laurent, Frédéric Alexandre, and Thierry Viéville. *Ontology  as neuronal-space manifold: towards symbolic and numerical artificial embedding*. In KRHCAI-21 (Knowledge Representation for Hybrid and Compositional AI). (submitted)
